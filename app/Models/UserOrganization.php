<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOrganization extends Model
{
    use HasFactory;
    protected $guarded=[];
    var $table = 'user_organizations';

    function user() {
        return $this->belongsTo(User::class);
    }

    function organization() {
        return$this->belongsTo(Organization::class);
    }

}
