<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::middleware(['role:Master'])->resource('users',\App\Http\Controllers\UserController::class);

//Worker Page
Route::middleware(['role:Master|Worker'])->get("/worker", function () {
    return View::make("worker");
})->name('worker-page');

//Agency Page
Route::middleware(['role:Master|Agency'])->get("/agency", function () {
    return View::make("agency");
})->name('agency-page');

//Manager Page
Route::middleware(['role:Master|Manager'])->get("/manager", function () {
    return View::make("manager");
})->name('manager-page');
