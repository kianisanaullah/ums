<?php

namespace Database\Seeders;

use App\Models\Organization;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        //Create Roles
        $worker_role = Role::create(['name' => 'Worker']);
        $agency_role = Role::create(['name' => 'Agency']);
        $manager_role = Role::create(['name' => 'Manager']);
        $master_role = Role::create(['name' => 'Master']);

        // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create demo users
        $master_user = \App\Models\User::factory()->create([
            'name' => 'Master User',
            'email' => 'master@example.com',
            'password' => bcrypt('123456')
        ]);
        $master_user->assignRole($master_role);
        $organizations=Organization::all();
        foreach ($organizations as $o) {
            $master_user->organizations()->attach($o);
        }

        $worker_user = \App\Models\User::factory()->create([
            'name' => 'Worker User',
            'email' => 'worker@example.com',
            'password' => bcrypt('123456')
        ]);
        $worker_user->assignRole($worker_role);

        $agency_user = \App\Models\User::factory()->create([
            'name' => 'Agency User',
            'email' => 'agency_user@example.com',
            'password' => bcrypt('123456')
        ]);
        $agency_user->assignRole($agency_role);

        $manager_user = \App\Models\User::factory()->create([
            'name' => 'Manager',
            'email' => 'manager@example.com',
            'password' => bcrypt('123456')
        ]);
        $manager_user->assignRole($manager_role);
    }
}
