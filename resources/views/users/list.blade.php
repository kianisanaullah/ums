@extends('layouts.app')

@section('content')
    <!-- select2 css -->
    <link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>

    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Users</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Users</a></li>
                                <li class="breadcrumb-item active">List</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @include('users.search')
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @if(count($errors)>0)
                                @foreach ($errors->all() as $error)
                                    <p class="alert alert-danger">{{ $error }}</p>
                                @endforeach
                            @endif
                            <h2 class="card-title">Users List</h2>
                            <!-- Extra Large modal -->
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <a class="btn btn-success waves-effect waves-light" data-bs-toggle="modal"
                                       data-bs-target=".bs-example-modal-xl"><i class="mdi mdi-plus me-2"></i> Add
                                        New</a>
                                </div>
                            </div>
                            @include('users.create')
                            <table id="datatable-buttons"
                                   class="table "
                                   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $user)
                                    <tr>
                                        <td><input type="checkbox" name="user_checkbox[]" class="user_checkbox" value="{{$user->id}}" /></td>
                                        <td>{{$user->id}}</td>
                                        <td>
                                            @if(!is_null($user->avatar))
                                                <img src="{{url('storage/'.$user->avatar)}}" alt=""
                                                     class="avatar-xs rounded-circle me-2">
                                            @else
                                                <div class="avatar-xs d-inline-block me-2">
                                                    <div
                                                        class="avatar-title bg-soft-primary rounded-circle text-primary">
                                                        <i class="mdi mdi-account-circle m-0"></i>
                                                    </div>
                                                </div>
                                            @endif
                                            <a href="#" class="text-body">{{$user->name}}</a>
                                        </td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone_no}}</td>
                                        <td>{{$user->roles->pluck('name')[0]}}</td>
                                        <td>
                                            @if($user->enabled==1)
                                                <span class="btn btn-success ">Yes</span>
                                            @else
                                                <span class="btn btn-danger ">No</span>
                                            @endif

                                            {{$user->status}}</td>
                                        <td>
                                            <ul class="list-inline mb-0">
{{--                                                <li class="list-inline-item">--}}
{{--                                                    <a href="javascript:void(0);" class="px-2 text-primary"><i--}}
{{--                                                            class="uil uil-pen font-size-18"></i></a>--}}
{{--                                                </li>--}}
                                                <li class="list-inline-item">
                                                    <form action="{{route('users.destroy',$user->id)}}" method="post">
                                                        {{ method_field('delete') }}
                                                        @csrf
                                                        <button class="px-2 text-danger"
                                                                onclick="return confirm('Are you sure?');"
                                                                type="submit"><i
                                                                class="uil uil-trash-alt font-size-18"></i></button>

                                                    </form>
                                                </li>
                                                {{--                                                <li class="list-inline-item dropdown">--}}
                                                {{--                                                    <a class="text-muted dropdown-toggle font-size-18 px-2" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true">--}}
                                                {{--                                                        <i class="uil uil-ellipsis-v"></i>--}}
                                                {{--                                                    </a>--}}

                                                {{--                                                    <div class="dropdown-menu dropdown-menu-end">--}}
                                                {{--                                                        <a class="dropdown-item" href="#">Action</a>--}}
                                                {{--                                                        <a class="dropdown-item" href="#">Another action</a>--}}
                                                {{--                                                        <a class="dropdown-item" href="#">Something else here</a>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                </li>--}}
                                            </ul>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
@endsection
@section('custom_scripts')
    <!-- Required datatable js -->
    <script src="{{asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-select-bs4/js/select.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>

    <!-- Responsive examples -->
    <script src="{{asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
    <!-- select 2 plugin -->
    <script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
    <!-- Datatable init js -->
    <script src="{{asset('assets/js/pages/datatables.init.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <script language="JavaScript">
        Webcam.set({
            width: 240,
            height: 120,
            image_format: 'jpg',
            jpeg_quality: 100
        });
        Webcam.attach('#my_camera');

        function take_snapshot() {
            Webcam.snap(function (data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="' + data_uri + '"/>';
            });
        }

        // Select2
        $(".select2").select2(
            {
                dropdownParent: $('#addUserModal'),
                width: '100%'
            }
        );

    </script>
@endsection
