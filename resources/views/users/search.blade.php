<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('users.index')}}" method="get" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label"
                                       for="formrow-email-input">Email</label>
                                <input type="email" class="form-control"
                                        name="email"
                                       @if(isset($request->email)) value="{{$request->email}}" @endif  >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label"
                                       for="formrow-email-input">Mobile</label>
                                <input type="text" class="form-control"
                                       id="formrow-email-input" name="mobile"
                                       placeholder="Mobile" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label"
                                       for="formrow-password-input">Role</label>
                                <select name="role_id" id="" class="form-control" >
                                    <option value="" selected>Select Role</option>
                                    @foreach(\Spatie\Permission\Models\Role::all() as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="" style="float: right">
                        <button type="submit"
                                class="btn btn-primary waves-effect waves-light w-md">Search
                        </button>
                        <a href="{{route('users.index')}}" class="btn btn-danger" >Reset</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
