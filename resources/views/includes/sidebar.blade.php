<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <a href="{{route('home')}}" class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="{{asset('assets/images/logo-sm.png')}}" alt="" height="22">
                        </span>
            <span class="logo-lg">
                            <img src="{{asset('assets/images/logo-dark.png')}}" alt="" height="20">
                        </span>
        </a>
        <a href="{{route('home')}}" class="logo logo-light">
                        <span class="logo-sm">
                            <img src="{{asset('assets/images/logo-sm.png')}}" alt="" height="22">
                        </span>
            <span class="logo-lg">
                            <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="20">
                        </span>
        </a>
    </div>
    <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect vertical-menu-btn">
        <i class="fa fa-fw fa-bars"></i>
    </button>
    <div data-simplebar class="sidebar-menu-scroll">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>
                <li>
                    <a href="{{route('home')}}">
                        <i class="uil-home-alt"></i>
                        {{--                        <span class="badge rounded-pill bg-primary float-end">01</span>--}}
                        <span>Dashboard</span>
                    </a>
                </li>
                @hasrole('Master')
                <li>
                    <a href="{{route('users.index')}}">
                        <i class="uil-users-alt"></i>
                        <span>Users</span>
                    </a>
                </li>
                @endhasrole
                @hasanyrole('Master|Worker')
                <li>
                    <a href="{{route('worker-page')}}">
                        <i class=" uil-post-stamp "></i>
                        <span>Worker</span>
                    </a>
                </li>
                @endhasanyrole
                @hasanyrole('Master|Manager')
                <li>
                    <a href="{{route('manager-page')}}">
                        <i class=" uil-monitor-heart-rate "></i>
                        <span>Manager</span>
                    </a>
                </li>
                @endhasanyrole
                @hasanyrole('Master|Agency')
                <li>
                    <a href="{{route('agency-page')}}">
                        <i class=" uil-mountains "></i>
                        <span>Agency</span>
                    </a>
                </li>
                @endhasanyrole
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
