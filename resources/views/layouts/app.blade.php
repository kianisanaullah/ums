<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>{{env('APP_NAME')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description"/>
    <meta content="Themesbrand" name="author"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
    @include('includes.styles')
</head>
<body>
<!-- <body data-layout="horizontal" data-topbar="colored"> -->
<!-- Begin page -->
<div id="layout-wrapper">
    @include('includes.topbar')
    @include('includes.sidebar')
    <div class="main-content">
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        @yield('content')
        <!-- end main content-->
        @include('includes.footer')
    </div>
</div>
<!-- END layout-wrapper -->
@include('includes.rightbar')
<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('includes.scripts')
@yield('custom_scripts')
@include('sweetalert::alert')
</body>
</html>
