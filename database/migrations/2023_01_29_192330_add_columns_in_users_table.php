<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone_no')->nullable();
            $table->string('avatar')->nullable();
            $table->string('surname')->nullable();
            $table->text('address1')->nullable();
            $table->text('address2')->nullable();
            $table->string('town')->nullable();
            $table->string('postcode')->nullable();
            $table->boolean('fire_marshall')->nullable();
            $table->boolean('first_aid')->nullable();
            $table->boolean('enabled')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone_no');
            $table->dropColumn('avatar');
            $table->dropColumn('surname');
            $table->dropColumn('address1');
            $table->dropColumn('address2');
            $table->dropColumn('town');
            $table->dropColumn('postcode');
            $table->dropColumn('fire_marshall');
            $table->dropColumn('first_aid');
            $table->dropColumn('enabled');
        });
    }
};
