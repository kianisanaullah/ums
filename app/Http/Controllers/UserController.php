<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
//        $this->middleware(['role:Super-Admin|Document Admin'],['except' => ['index']]);
        $this->middleware(['role:Master'],['only' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query=User::query();
        if (isset($request->email))
        {
            $query->where('email','like',"%$request->email%");
        }
        if (isset($request->role_id))
        {
            $query->whereRoleId($request->role_id);
        }
        $users=$query->get();
        return view('users.list',compact('users','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'password' => 'required',
            'email' => 'required|unique:users|max:255',
            'phone_no' => 'required',

//            'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048'
        ]);
        $img = $request->image;
        $folderPath = "user_avatars/";
        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.'.$image_type;
        $file = $folderPath . $fileName;
        Storage::disk('public')->put($file, $image_base64);
        $data=$request->except('image','_token','password','permissions','organizations','enabled','fire_marshall','first_aid','role_id');
        if (isset($request->first_aid)){
            $data['first_aid']=1;
        }if (isset($request->first_aid)){
            $data['enabled']=1;
        }if (isset($request->first_aid)){
            $data['fire_marshall']=1;
        }

        $data['avatar']=$folderPath . $fileName;
        $data['password']=Hash::make($request->password);
        $user=User::create($data);
        $user->assignRole($request->role_id);
        if (isset($request->permissions))
        {
            $permissions=$request->permissions;
            foreach ($permissions as $p) {
                $permission=Permission::findById($p);
                $user->givePermissionTo($permission);
            }
        }
        if (isset($request->organizations))
        {
            $organizations=$request->organizations;
            foreach ($organizations as $o) {
                $user->organizations()->attach($o);
            }
        }
        alert('Success','User Created Successfully','success');
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();
        alert('Deleted','User Deleted','danger');
        return redirect()->route('users.index');
    }
}
