<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $table='organizations';
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_organizations', 'organization_id', 'user_id');
    }

}
