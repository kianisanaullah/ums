<?php

namespace Database\Seeders;

use App\Models\Organization;
use Illuminate\Database\Seeder;

class OrganizationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create organizations
        Organization::create(['name' => 'Organization A']);
        Organization::create(['name' => 'Organization B']);
        Organization::create(['name' => 'Organization C']);

    }
}
