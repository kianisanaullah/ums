<!--  Extra Large modal example -->
<div class="modal fade bs-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="addUserModal"
     aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addUserModal">Add User Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="my_camera" style="width: 200px !important;"></div>
                                    <br/>
                                    <input type=button value="Take Snapshot" onClick="take_snapshot()">
                                    <input type="hidden" name="image" class="image-tag">
                                </div>
                                <div class="col-md-6">
                                    <div id="results">Your captured image will appear here...</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="formrow-Fullname-input">Name</label>
                                            <input type="text" class="form-control" id="formrow-Fullname-input"
                                                   placeholder="Name" name="name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <div class="mb-3">
                                            <label class="form-label">Surname</label>
                                            <input type="text" class="form-control"
                                                   placeholder="Surname" name="surname" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label"
                                               for="formrow-email-input">Email</label>
                                        <input type="email" class="form-control"
                                               id="formrow-email-input" name="email"
                                               placeholder="Email address" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label"
                                               for="formrow-password-input">Password</label>
                                        <input type="password" name="password" class="form-control"
                                               id="formrow-password-input"
                                               placeholder="Password" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label"
                                               for="formrow-email-input">Mobile</label>
                                        <input type="text" class="form-control"
                                               id="formrow-email-input" name="phone_no"
                                               placeholder="Mobile" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label"
                                               for="formrow-password-input">Role</label>
                                        <select name="role_id" id="" class="form-control" required>
                                            <option value="" disabled readonly>Select Role</option>
                                            @foreach(\Spatie\Permission\Models\Role::all() as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label"
                                               for="formrow-email-input">Address 1</label>
                                        <input type="text" class="form-control"
                                               id="formrow-email-input" name="address1"
                                               placeholder="Address 1" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label"
                                               for="formrow-email-input">Address 2</label>
                                        <input type="text" class="form-control"
                                               name="address2"
                                               placeholder="Address 2" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label"
                                               for="formrow-email-input">Town</label>
                                        <input type="text" class="form-control"
                                               name="town"
                                               placeholder="Town" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label"
                                               for="formrow-email-input">Post Code</label>
                                        <input type="text" class="form-control"
                                               name="postcode"
                                               placeholder="Post Code" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-3">
                                        <div class="form-check form-switch form-switch-md">
                                            <input type="checkbox" name="enabled" class="form-check-input" >
                                            <label class="form-check-label" >Enabled</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-3">
                                        <div class="form-check form-switch form-switch-md">
                                            <input type="checkbox" class="form-check-input"
                                                   id="fire_marshall-customCheck" name="fire_marshall" checked>
                                            <label class="form-check-label" for="formrow-customCheck">Fire Marshall</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-3">
                                        <div class="form-check form-switch form-switch-md">
                                            <input type="checkbox" class="form-check-input"
                                                   id="first_aid-customCheck" name="first_aid" checked>
                                            <label class="form-check-label" for="formrow-customCheck">First Aid</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label"
                                               for="formrow-email-input">Organizations</label>
                                        <select name="organizations[]" class="select2 form-control select2-multiple" multiple="multiple" data-placeholder="Choose ...">
                                            @foreach(\App\Models\Organization::all() as $organization)
                                                <option value="{{$organization->id}}">{{$organization->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="d-flex flex-wrap gap-3 mt-3">
                                <button type="submit"
                                        class="btn btn-primary waves-effect waves-light w-md">Submit
                                </button>
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <h4 class="mb-0">Permissions</h4>
                        @foreach(\Spatie\Permission\Models\Permission::all() as $permission)
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">{{$permission->name}}</label>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input"
                                               id="{{$permission->name}}-customCheck" name="permissions[]">
                                        <label class="form-check-label"
                                               for="{{$permission->name}}-customCheck">Yes</label>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

