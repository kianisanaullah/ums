@extends('layouts.app')

@section('styles')


@endsection

@section('content')

    <!-- BREADCRUMB -->
    <div class="breadcrumb-header justify-content-between">
        <div class="left-content">
            <div>
                <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">Hi, welcome back!</h2>
                <p class="mg-b-0">Sales monitoring dashboard template.</p>
            </div>
        </div>
        <div class="main-dashboard-header-right">
            <div>
                <label class="tx-13">Customer Ratings</label>
                <div class="main-star">
                    <i class="typcn typcn-star active"></i> <i class="typcn typcn-star active"></i> <i
                        class="typcn typcn-star active"></i> <i class="typcn typcn-star active"></i> <i
                        class="typcn typcn-star"></i> <span>(14,873)</span>
                </div>
            </div>
            <div>
                <label class="tx-13">Online Sales</label>
                <h5>563,275</h5>
            </div>
            <div>
                <label class="tx-13">Offline Sales</label>
                <h5>783,675</h5>
            </div>
        </div>
    </div>
    <!-- END BREADCRUMB -->

    <!-- ROW -->
    <div class="row row-sm">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                @if (session()->has('message'))
                    <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                         role="alert">
                        <div class="flex">
                            <div>
                                <p class="text-sm">{{ session('message') }}</p>
                            </div>
                        </div>
                    </div>
                @endif
                <button wire:click="create()"
                        class="my-4 inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base font-bold text-white shadow-sm hover:bg-red-700">
                    Create Student
                </button>
                @if($isModalOpen)
                    @include('users.create')
                @endif
                <table class="table-fixed w-full">
                    <thead>
                    <tr class="bg-gray-100">
                        <th class="px-4 py-2 w-20">No.</th>
                        <th class="px-4 py-2">Name</th>
                        <th class="px-4 py-2">Email</th>
                        <th class="px-4 py-2">Mobile</th>
{{--                        <th class="px-4 py-2">Action</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td class="border px-4 py-2">{{ $user->id }}</td>
                            <td class="border px-4 py-2">{{ $user->name }}</td>
                            <td class="border px-4 py-2">{{ $user->email}}</td>
{{--                            <td class="border px-4 py-2">{{ $user->mobile}}</td>--}}
                            <td class="border px-4 py-2">
                                <button wire:click="edit({{ $user->id }})"
                                        class="flex px-4 py-2 bg-gray-500 text-gray-900 cursor-pointer">Edit</button>
                                <button wire:click="delete({{ $user->id }})"
                                        class="flex px-4 py-2 bg-red-100 text-gray-900 cursor-pointer">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
    </div>

@endsection

@section('scripts')

    <!--INTERNAL CHART.BUNDLE JS -->
    <script src="{{asset('build/assets/plugins/chart.js/Chart.bundle.min.js')}}"></script>

    <!--INTERNAL SPARKLINE JS -->
    <script src="{{asset('build/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- RAPHAEL JS -->
    <script src="{{asset('build/assets/plugins/raphael/raphael.min.js')}}"></script>

    <!-- INTERNAL APEXCHART JS -->
    @vite('resources/assets/js/apexcharts.js')


    <script src="{{asset('build/assets/jquery.vmap.sampledata.js')}}"></script>

    <!-- MODAL POPUP JS -->
    @vite('resources/assets/js/modal-popup.js')


    <!-- INTERNAL MAP JS -->
    <script src="{{asset('build/assets/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('build/assets/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>

    <!-- INTERNAL INDEX JS -->
    @vite('resources/assets/js/index.js')

    @vite('resources/assets/js/index1.js')

@endsection
